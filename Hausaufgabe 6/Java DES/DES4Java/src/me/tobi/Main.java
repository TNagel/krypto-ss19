package me.tobi;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        String x1_R = "00101110101010100111100110011101";
        String x1_E = Util.getEBitPermutation(x1_R);
        System.out.println("X1-E:" + x1_E);

        String y1_R = "00110011001010010010010011011000";
        String x1_L = "11110101011111011101110110110001";
        String y1_R_XOR_y1_L = Util.XORBinaryString(y1_R, x1_L);
        System.out.println("XOR: " + y1_R_XOR_y1_L);

        String invP1 = Util.getInversePPermutation(y1_R_XOR_y1_L);
        System.out.println("P invers: " + invP1);

        System.out.println("\nX2:");

        String x2_R = "11010101001000000010010000110111";
        String x2_E = Util.getEBitPermutation(x2_R);
        System.out.println("X2-E:" + x2_E);

        String y2_R = "10110101110110001111110000110001";
        String x2_L = "00010101100000110111100101110111";
        String y2_R_XOR_y2_L = Util.XORBinaryString(y2_R, x2_L);
        System.out.println("XOR: " + y2_R_XOR_y2_L);

        String invP2 = Util.getInversePPermutation(y2_R_XOR_y2_L);
        System.out.println("P invers: " + invP2);

        //String invF_Perm = Utils.
    }
}
