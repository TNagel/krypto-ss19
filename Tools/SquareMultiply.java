import java.lang.*;

public class SquareMultiply {

	public static void main(String[] args) {
		if(args.length == 3) {
			try {
				int a = Integer.valueOf(args[0]);
				int b = Integer.valueOf(args[1]);
				int m = Integer.valueOf(args[2]);
				//a^b mod m
				
				//result of each calculation
				long result = a % m;
				
				//calculate binary exponent
				String binary = Integer.toBinaryString(b);
				char[] bits = binary.toCharArray();
				
				System.out.println("Binary Exponent: " + String.valueOf(bits));
				
				String bitstr = "1";
				System.out.println("\\begin{tabular}{l | l}");
				for(int i = 1; i < bits.length; i++) {
					//System.out.println("result=" + result);
					if(bits[i] == '0') {
						//SQ
						bitstr += 0;
						System.out.print(bitstr + "&$" + result + "^2 \\equiv");
						result = result*result;
						result %= m;
						System.out.println(result + "~mod~" + m + "$ \\\\");
					}else if(bits[i] == '1') {
						//SQ 
						System.out.print(bitstr + "0&$" + result + "^2 \\equiv");
						result = result*result;
						result %= m;
						
						bitstr += 1;
						System.out.println(result + "~mod~" + m + "$ \\\\");
						//MUL
						System.out.print(bitstr + "&$" + result + "\\cdot " + a + "\\equiv");
						result *= a;
						result %= m;
						System.out.println(result + "~mod~" + m + "$ \\\\");
					}else {
						throw new RuntimeException("Wrong Bit");
					}
				}
				
				System.out.println("\\end{tabular}");
				
			}catch(NumberFormatException e) {
				System.out.println("Arguments must be numbers.");
			}
		}else {
			System.out.println("Arguments: <Base> <Exponent> <Modulus>");
		}
	}

}